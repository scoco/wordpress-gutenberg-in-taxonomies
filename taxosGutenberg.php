<?php



/**
 * Taxonomies with gutenberg
 *
 * @since 5.8.2
 */
class taxosGutenberg
{
    public $prefixCPT = "mirror_";
    public $taxonomies = [
        'activites'
    ];

    function __construct()
    {
        add_action('init', [$this, 'init_CPTclone']);


        if (!wp_next_scheduled('taxoData_to_CPT_hook')) {
            wp_schedule_event(time(), 'twicedaily', 'taxoData_to_CPT_hook');
        }
        add_action('taxoData_to_CPT_hook', [$this, 'taxoData_to_CPT']);

        // add_action('save_post', [$this, 'cptData_to_TAXO'], 10, 3);

        add_filter('pre_get_posts', [$this, 'change_content_taxos']);
    }

    /**
     *
     * Find the different taxonomies and clone in Custom Post Type
     *
     */
    public function init_CPTclone()
    {
        if (!empty($this->taxonomies) && is_array($this->taxonomies)) {

            $taxonomies = get_taxonomies();
            foreach ($taxonomies as $tax_type_key => $taxonomy) {
                if (in_array($tax_type_key, $this->taxonomies)) {

                    $taxo_args = get_taxonomy($tax_type_key);
                    if (!empty($taxo_args)) {

                        $this->register_CPTclone($tax_type_key, $taxo_args->object_type);

                        $this->hide_menuTaxo($tax_type_key, $taxo_args);
                    }
                }
            }
        }
    }

    /**
     *
     * Save the Custom Post Type
     *
     * @param    string  $name Name/Slug of Custom Post Type
     * @param    array  $cpt_associated List of Custom Post Type
     *
     */
    public function register_CPTclone($name, $cpt_associated)
    {
        $show_in_menu = true;
        if (!empty($cpt_associated[0])) {
            $show_in_menu = 'edit.php?post_type=' . $cpt_associated[0];
        }


        if (!empty($name)) {
            $labels = array(
                'name' => _x($name, 'Post Type General Name', 'taxos_with_gut'),
                'singular_name' => _x($name, 'Post Type Singular Name', 'taxos_with_gut'),
                'menu_name' => _x($name, 'Admin Menu text', 'taxos_with_gut'),
            );
            $args = array(
                'label' => __($name, 'taxos_with_gut'),
                'description' => __('CPT Mirror of ' . $name, 'taxos_with_gut'),
                'labels' => $labels,
                'menu_icon' => 'dashicons-admin-network',
                'supports' => array('title', 'editor'),
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => $show_in_menu,
                'menu_position' => 100,
                'show_in_admin_bar' => false,
                'show_in_nav_menus' => false,
                'can_export' => false,
                'has_archive' => false,
                'hierarchical' => false,
                'exclude_from_search' => false,
                'show_in_rest' => true,
                'publicly_queryable' => false,
                'capability_type' => 'post',
                'rewrite' => true,
            );
            register_post_type($this->prefixCPT . $name, $args);
        }
    }

    /**
     *
     * Hide affected taxonomies
     *
     * @param    string  $taxo_name Name/Slug of Taxonomy
     * @param    object  $taxo_args Arguments for registering a taxonomy.
     *
     */
    public function hide_menuTaxo($taxo_name, $taxo_args)
    {
        $taxo_args->show_in_menu = false;
        $taxo_args->show_in_nav_menus = false;
        // Re-register la taxo
        register_taxonomy($taxo_name, $taxo_args->object_type, $taxo_args);
    }

    /**
     *
     * copy the data of a taxonomy in a Custom Post Type
     *
     */
    public function taxodata_to_cpt()
    {
        if (!empty($this->taxonomies) && is_array($this->taxonomies)) {
            foreach ($this->taxonomies as $taxonomy) {
                $terms = get_terms([
                    'taxonomy' => $taxonomy,
                ]);

                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        $post_exist = get_page_by_title($term->name, OBJECT, $this->prefixCPT . $taxonomy);
                        if ($post_exist === NULL) {
                            $post_data = array(
                                'post_title' => wp_strip_all_tags($term->name),
                                'post_content' => wp_strip_all_tags($term->description),
                                'post_type' => $this->prefixCPT . $taxonomy,
                                'post_name' => $this->prefixCPT . $term->slug
                            );
                            $post_id = wp_insert_post($post_data);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * copy the data of a Custom Post Type into a taxonomy
     * @param int $post_id — Name/Slug of Taxonomy
     * @param object $post — Arguments for registering a taxonomy.
     * @param object $update — Arguments for registering a taxonomy.
     *
     */
    public function  cptData_to_TAXO($post_id, $post, $update)
    {
        if (!$update) {
            return;
        }
        if (wp_is_post_revision($post_id)) {
            return;
        }
        if (defined('DOING_AUTOSAVE') and DOING_AUTOSAVE) {
            return;
        }
        if (strpos($post->post_type, $this->prefixCPT) !== false) {
            $term_slug = str_replace($this->prefixCPT, "", $post->post_name);
            $taxo_slug = str_replace($this->prefixCPT, "", $post->post_type);
            $term = get_term_by('slug', $term_slug, $taxo_slug);
            wp_update_term($term->term_id, $taxo_slug, ['description' => $post->content->rendered]);
        }
    }

    /**
     *
     * Change the content of a taxonomy before rendering
     * @param object $query current query
     *
     */
    public function change_content_taxos($query)
    {
        if (!is_admin() && is_tax($this->taxonomies) && $query->is_main_query()) {
            if (!empty($query->queried_object)) {
                $post_exist = get_page_by_title($query->queried_object->name, OBJECT, $this->prefixCPT . $query->queried_object->taxonomy);
                if ($post_exist !== NULL) {
                    $query->get_queried_object()->description = get_the_content(null, false, $post_exist->ID);
                }
            }
        }
        return $query;
    }
}


$tGut = new taxosGutenberg();
